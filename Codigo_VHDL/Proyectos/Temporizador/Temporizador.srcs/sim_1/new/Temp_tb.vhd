----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 26.11.2019 10:59:45
-- Design Name: 
-- Module Name: Temp_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Temp_tb is
end Temp_tb;

architecture behavior of Temp_tb is 
  constant ClockPeriod      : time := 1 sec; --Ajustar este parametro a nuestro periodo
  constant Nbit      : positive := 5;
  signal Load: std_logic;
  signal Tiempo: std_logic_vector( NBit - 1 downto 0);
  signal Clk: std_logic:= '0';
  signal Reset:  std_logic;
  signal Seconds: std_logic_vector( NBit - 1 downto 0);
  signal Ready: std_logic;
  signal Estado:  integer range 0 tO 2;
    component Temp is
        port (
                Clk: in std_logic := '0';
                Tiempo:in std_logic_vector( NBit - 1 downto 0);
                Reset: in std_logic:='1';
                Seconds:out std_logic_vector( NBit - 1 downto 0);
                Load:in std_logic;
                 Estado: out integer range 0 tO 2;
                Ready:out std_logic
             );
    end component;
begin
    uut: Temp port map (
        Clk => Clk,
        Reset => Reset,
        Load => Load,
        Tiempo => Tiempo,
        Estado=>Estado,
        Seconds => Seconds,
        Ready=>Ready
    );
    Clk <= not Clk after 0.5 * ClockPeriod;
    
    p1:process
    begin
      Reset <= '1', '0' after 0.25 * ClockPeriod;
      Tiempo <= "01010"; --Cargamos 10 en tiempo
      wait for 6.5*ClockPeriod;
      Load<='1';--,'0' after 0.05 * ClockPeriod;
     wait for 0.0005*ClockPeriod;
     Load<='0';
--     Load<='1','0' after 0.05 * ClockPeriod;
      wait;
   end process;
    
end;
