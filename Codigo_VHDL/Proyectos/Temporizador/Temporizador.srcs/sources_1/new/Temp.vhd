----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 25.11.2019 21:30:40
-- Design Name: 
-- Module Name: Temp - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Temp is
generic(
    NBit:positive:=5
    );
  Port ( 
  Load: in std_logic;
  Tiempo:in std_logic_vector( NBit - 1 downto 0);
  Clk: in std_logic;
  Reset: in std_logic;
  Seconds:out std_logic_vector( NBit - 1 downto 0);
  Ready:out std_logic--Se activa cuando la cuenta llega a un determinado numero de segundos
  ); 
end Temp;


architecture behavioral of Temp is
  signal count: unsigned(Tiempo'range);
  signal Seconds_i:unsigned(Seconds'range);
begin
  process (Reset,Clk,Load)
  begin
    if Reset = '1' then
      count <= (others => '0');
      Seconds_i <= (others => '0');
    elsif  Load = '1' and count = 0 then
        count <= unsigned(Tiempo);
    elsif Clk'event and Clk = '1' then
        if count /= 0 then
            count <= count - 1;
            Seconds_i <= Seconds_i + 1;
        end if;
    end if;
  end process;

  Ready <= '1' when count = 0 else
       '0';
  Seconds<=std_logic_vector(Seconds_i);
end behavioral;

--architecture Behavioral of Temp is
--type state_type IS (S0, S1,S2);-- S0 inicial S1 contando S2 ready
--signal state, next_state: state_type;
--signal Seconds_i: integer range 0 to 31;
--signal Ready_i:std_logic;
--begin
--    SYNC_PROC: PROCESS (Clk,Reset)
--    BEGIN
--        IF Reset = '1' THEN
--                state <= S0;  
--        ELSIF rising_edge(clk) THEN
--                state <= next_state;
--        END IF;
--    END PROCESS;
    
    
--    OUTPUT_DECODE: PROCESS (Clk,state)
--    BEGIN
--        CASE (state) is
--             WHEN S0 => 
--                  Seconds_i<=0;
--                  Ready_i <= '0';
--                  Estado<=0;
--             WHEN S1 => 
--                IF rising_edge(clk)  THEN
--                    Seconds_i<=Seconds_i + 1;
--                end if;
--                 Estado<=1;
--              WHEN S2 =>
--                Ready_i<='1';
--                 Estado<=2;
--        END CASE;
--    END PROCESS;


--    NEXT_STATE_DECODE: PROCESS (state,Load,Tiempo,Seconds_i)
--    BEGIN
--       -- next_state <= S0;
--        CASE (state) is
--            WHEN S0 =>
--                IF Load = '1' THEN
--                    next_state <= S1;
--                ELSE
--                 next_state <= S0;
--                END IF;
--            WHEN S1 =>
--               IF Seconds_i = to_integer(unsigned(Tiempo)) THEN
--                   next_state <= S2;
--               ELSE
--                   next_state <= S1;
--               END IF;
--            WHEN S2 => 
--                next_state <= S0;
--           END CASE;
--     END PROCESS; 
-- Seconds<=std_logic_vector(to_unsigned(Seconds_i, Seconds'length));
-- Ready<=Ready_i;
--end Behavioral;





--library IEEE;
--use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.numeric_std.all;
 
--entity Temp is
--   generic(
--    NBit:positive:=5
--    );
--  Port ( 
--    Load: in std_logic;
--    Tiempo:in std_logic_vector( NBit - 1 downto 0);
--    Clk: in std_logic;
--    Reset: in std_logic;
--    Seconds:out std_logic_vector( NBit - 1 downto 0);
--    Estado: out integer range 0 tO 2;
--    Ready:out std_logic--Se activa cuando la cuenta llega a un determinado numero de segundos
--  );
--end Temp;
 
--architecture Behavioral of Temp is
--  type estados_t is (S0,S1,S2);
--  signal state, next_state : estados_t; 
--begin
--  process(Load,state,Clk,Tiempo)
--  subtype count_T is integer range 0 to 31;
--  variable cnt_temp:count_T;
--  begin
--    --next_state <= state;
--    case(state) is
--      when S0 =>
--        Ready <= '0';
--        Estado<=0;
--        Seconds<=(others => '0');
--        cnt_temp:=0;
--        if(Load='1') then next_state <=S1;
--        elsif(Load='0') then next_state <= S0;
--        end if;
--      when S1 =>
--        Ready <= '0';
--        Estado<=1;
--        if rising_edge(clk) then
--         cnt_temp:=cnt_temp+1;
--        end if;
--        if(cnt_temp=to_integer(unsigned(Tiempo))) then next_state <= S2;
--        else next_state <= S1;
--        end if;
--        Seconds<=std_logic_vector(to_unsigned(cnt_temp, Seconds'length));
--      when S2 =>
--        Ready <= '1';
--        Estado<=2;
--        next_state <= S0;
--      when others => null;
--    end case;
--  end process;
  
--  process (Clk,Reset) begin
--    if(Reset='1') then
--      state <= S0;
--    elsif rising_edge(clk) then
--      state <= next_state; 
--    end if;
--  end process;
--end Behavioral;






