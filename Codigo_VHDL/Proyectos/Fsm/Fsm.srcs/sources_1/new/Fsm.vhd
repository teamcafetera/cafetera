----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 25.12.2019 19:23:54
-- Design Name: 
-- Module Name: Fsm - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Fsm is
  generic(
    NBit_Tiempo:positive:=5;
    NOpc_Flick:positive:=2; --Numero de opciones de flicker
    N_BitOpc_Displ:positive:=3 --Numero de bits para las 5 opciones de los displays
    );
    
  Port ( 
  Reset: in std_logic;
  Clk: in std_logic;
  Start: in std_logic;
  Timer_Ready: in std_logic;
  Boton_Corto: in std_logic;
  Boton_Largo: in std_logic;
  Sensor_Agua: in std_logic; -- Nivel alto si hay agua
  Sensor_Vaso: in std_logic;
  Sensor_Capsula: in std_logic;
  Led_Cafetera_Encendida: out std_logic;
  Led_No_Capsula: out std_logic; --Encendido si no esta metida la capsula
  Led_No_Vaso: out std_logic;--Encendido si no esta puesto el vaso
  Led_Eleccion_Cafe: out std_logic; ---Se enciende este led cuando la cafetera esta lista para empezar a echar cafe
  Led_Cafe_Listo: out std_logic;
  Enable_Decoder: out std_logic;
  Enable_Flicker: out std_logic;
  Enable_Displays: out std_logic;
  Load_Timer: out std_logic;
  Tiempo:out std_logic_vector( NBit_Tiempo - 1 downto 0);
  Opcion_Flicker: out std_logic_vector (NOpc_Flick - 1 downto 0);
  Opcion_Displays: out std_logic_vector (N_BitOpc_Displ - 1 downto 0)
  --Estado: out integer range 0 to 11 --Esta variable se ha usado para controlar en que estado se encontraba la maquina y depurar codigo
  );
end Fsm;

architecture Behavioral of Fsm is
type state_type IS (S0,S1,S2,S31,S32,S4,S5,S6,S7,S8,S91,S92,S10,S11);-- S0 inicial S1 cafetera encendida S2 Carga temp calentar agua  S3 Espera calentando agua
-- S4 Comprobacion sensor capsula S5 Comprobacion sensor vaso   S6 Eleccion tipo cafe  S7 Carga temp cafe Corto 
--S8 Carga temp cafe Largo  S9 Espera Dispensando cafe S10 Cafe servido   De S10 retorna a S1  si no hay mas agua  o a S11 si hay mas agua
--S11 es un estado de espera para que cambies la capsula-> cuando el sensor de capsula pasa a ser 0 entonces retorna a S4
signal state, next_state: state_type;
begin
    SYNC_PROC: PROCESS (Clk)
    BEGIN
        IF rising_edge(clk) THEN
            IF Reset = '1' or Start = '0' THEN
                state <= S0;          
            ELSE
                state <= next_state;
               
            END IF;
        END IF;
    END PROCESS;
    
    
    OUTPUT_DECODE: PROCESS (state)
    BEGIN
        CASE state is
            WHEN S0 => 
                Led_Cafetera_Encendida<='0';
                Led_No_Capsula<='0';
                Led_No_Vaso<='0';
                Led_Eleccion_Cafe<='0';
                Led_Cafe_Listo<='0';
                Load_Timer<='0';
                Enable_Flicker<='0';
                Enable_Decoder<='0';
                Enable_Displays<='0';
                Opcion_Displays<="000";
                Opcion_Flicker<="00";
                Tiempo<="00000";
                -- Estado<=0;
               
         WHEN S1 =>   
                Led_Cafetera_Encendida<='1';
                Enable_Flicker<='1';
                Opcion_Flicker<="10"; -- Opcion 2: Ponemos a parpadear el led de la resistencia del agua porque no hay agua 
                Enable_Displays<='1';
                Opcion_Displays<="001"; -- Opcion 1: NO AGUA 
                Led_Cafe_Listo<='0';
                -- Estado<=1;
                
            WHEN S2 =>  
                Load_Timer<='1';
                Led_Cafetera_Encendida<='1';
                Enable_Flicker<='0';
                Enable_Displays<='0';
                Tiempo<="11110"; --Cargamos 30 seg para calentar agua
               --Estado<=2;
           
           WHEN S31 =>
                Load_Timer<='0';
                Led_Cafetera_Encendida<='1';
                Enable_Flicker<='1';
                Opcion_Flicker<="01"; --Opcion 1: que es el led de resistencia agua fijo
                --Estado<=3;
            WHEN S32 =>
                Load_Timer<='0';
                Led_Cafetera_Encendida<='1';
                Enable_Flicker<='1';
                Opcion_Flicker<="01"; --Opcion 1: que es el led de resistencia agua fijo
                --Estado<=3;
             
             WHEN S4 =>
                Enable_Flicker<='0';
                Led_Cafetera_Encendida<='1';
                Led_Cafe_Listo<='0';
                Enable_Displays<='1';
                Opcion_Displays<="010"; -- Opcion 2: NO CAPSUL 
                Led_No_Capsula<='1';
                --Estado<=4;
             
             WHEN S5 =>
               Led_No_Vaso<='1';
               Enable_Displays<='1';
               Opcion_Displays<="011"; -- Opcion 3: NO VASO
               Led_Cafetera_Encendida<='1';
               Led_No_Capsula<='0';
               -- Estado<=5;
             
             WHEN S6 =>
                Led_Eleccion_Cafe<='1';
                Led_No_Vaso<='0';
                Enable_Displays<='1';
                Opcion_Displays<="100"; -- Opcion 4: ELIGE
               -- Estado<=6;
             
             WHEN S7 =>
                Load_Timer<='1';
                Led_Cafetera_Encendida<='1';
                Tiempo<="01010"; --Cargamos 10 seg de cafe corto
                Led_Eleccion_Cafe<='0';
                Enable_Displays<='0';
                --Estado<=7;
             
             WHEN S8 =>
                Load_Timer<='1';
                Led_Cafetera_Encendida<='1';
                Tiempo<="10100"; --Cargamos 20 seg de cafe largo
                Led_Eleccion_Cafe<='0';
                Enable_Displays<='0';
                --Estado<=8;
             
             WHEN S91 => 
               Load_Timer<='0'; 
               Led_Cafetera_Encendida<='1';
               Enable_Decoder<='1';
               --Estado<=9;
             WHEN S92 => 
               Load_Timer<='0'; 
               Led_Cafetera_Encendida<='1';
               Enable_Decoder<='1';
            
             WHEN S10 => 
               Led_Cafe_Listo<='1';
               Enable_Displays<='1';
               Opcion_Displays<="101"; -- Opcion 5: LISTO
               Led_Cafetera_Encendida<='1';
               Enable_Decoder<='0';
              -- Estado<=10;
              WHEN S11 => 
               Led_Cafetera_Encendida<='1';
               Led_Cafe_Listo<='0';
               Led_No_Capsula<='1';
               Enable_Displays<='1';
               Opcion_Displays<="010"; -- Opcion 2: NO CAPSUL 
               

             
             
       END CASE;
    END PROCESS;


    NEXT_STATE_DECODE: PROCESS (state,Start,Sensor_Agua,Timer_Ready,Sensor_Capsula,Sensor_Vaso,Boton_Corto,Boton_Largo)
    BEGIN
       CASE state is
            WHEN S0 =>
                 IF (Start = '1') THEN
                     next_state <= S1;
                  ELSE
                      next_state <= S0;
                 END IF;    
           WHEN S1 =>
                 IF (Sensor_Agua = '1') THEN
                     next_state <= S2;
                 ELSE
                      next_state <= S1;
                 END IF;      
            WHEN S2 =>
                  next_state <= S31;
             WHEN S31 =>
                  IF  Timer_Ready = '0' THEN
                     next_state <= S32;
                  ELSE
                      next_state <= S31; 
                  END IF;
             WHEN S32 =>
                  IF  Timer_Ready = '1' THEN
                     next_state <= S4;
                  ELSE
                      next_state <= S32;
                  END IF;
             
             WHEN S4 =>-- Modificamos para poder hacer otro cafe necesitamos detectar el flanco de subida del sensor
                       --Este flanco indica que el usuario ha introducido una nueva capsula
                        --Esto esta implementado a�adiendo un estado mas porque los interruptores no permiten lectura de reloj
                 IF Sensor_Capsula = '1' THEN
                     next_state <= S5;
                 ELSE
                      next_state <= S4;
                  END IF;
                  
             WHEN S5 =>
                  IF Sensor_Vaso = '1' THEN
                         next_state <= S6;
                  ELSE
                      next_state <= S5;
                  END IF;
             
             WHEN S6 =>
                  IF Boton_Corto = '1' THEN
                     next_state <= S7;
                  ELSIF Boton_Largo = '1' THEN
                     next_state <= S8;
                  ELSE
                      next_state <= S6;
                  END IF;
             
             WHEN S7 =>
                  next_state <= S91;
             
             WHEN S8 =>
                  next_state <= S91;
             
             WHEN S91 =>
                  IF  Timer_Ready = '0' THEN
                        next_state <= S92;
                  ELSE
                      next_state <= S91;
                  END IF;
               WHEN S92 =>
                  IF  Timer_Ready = '1' THEN
                        next_state <= S10;
                  ELSE
                      next_state <= S92;
                  END IF;
              
              WHEN S10 =>
                   IF Sensor_Agua = '0' THEN
                        next_state <= S1;
                   ELSIF Sensor_Vaso = '0' THEN
                        next_state <= S11;
                   ELSE
                      next_state <= S10;
                   END IF;
              WHEN S11 =>
                   IF Sensor_Capsula = '0' THEN
                        next_state <= S4;
                   ELSE
                      next_state <= S11;
                   END IF;
           END CASE;
     END PROCESS; 

end Behavioral;

