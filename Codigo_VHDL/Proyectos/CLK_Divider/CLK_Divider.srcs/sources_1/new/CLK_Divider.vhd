----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09.12.2019 19:26:17
-- Design Name: 
-- Module Name: CLK_Divider - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity CLK_Divider is
    generic(
    semimodule:positive:=5*10e6
     );
    Port (
    Reset: in std_logic;
    CLK_in: in std_logic;
    CLK_out:out std_logic
     );
end CLK_Divider;

architecture Behavioral of CLK_Divider is
signal CLKout_I: std_logic;
begin
process(Reset, CLK_in)
    subtype count_T is integer range 0 to semimodule;
    variable count:count_T;
begin
    if Reset='1' then
        count:=count_T'high;
        CLKout_I<='0';
     elsif rising_edge(CLK_in) then
        count:=count-1;
        if count=0 then
            count:=count_T'high;
            CLKout_I<=not CLKout_I;
        end if;
     end if;
end process;
    CLK_out<=CLKout_I;
end Behavioral;
