----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 20.01.2020 16:20:06
-- Design Name: 
-- Module Name: Clk_Divider - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity Clk_Divider_2 is
    Generic (frec: integer:=125000);  -- default value is for 10hz
    Port ( clk_in : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           clk_out : out  STD_LOGIC);
end Clk_Divider_2;


architecture Behavioral of Clk_Divider_2 is
signal clk_sig: std_logic;
begin

  process (clk_in,reset)
  variable cnt:integer;
  begin
		if (reset='1') then
		  cnt:=0;
		  clk_sig<='0';
		elsif clk_in'event and clk_in='1' then
			if (cnt=frec) then
				cnt:=0;
				clk_sig<=not(clk_sig);
			else
				cnt:=cnt+1;
			end if;
		end if;
  end process;
  clk_out<=clk_sig;
end Behavioral;
