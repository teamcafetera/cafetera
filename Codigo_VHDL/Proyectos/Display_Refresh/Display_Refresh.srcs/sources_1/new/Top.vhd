----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 20.01.2020 15:58:41
-- Design Name: 
-- Module Name: Top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Top is
    generic(
          NBitOpc:positive:=3 --El Numero de opciones son 5 por tanto vector de 3 bits sirve
          );
    Port ( 
        Clk: in std_logic;
        Reset: in std_logic;
        Enable: in std_logic;
        Opcion: in std_logic_vector (NBitOpc - 1 downto 0);
        display_number : out  STD_LOGIC_VECTOR (6 downto 0);
        display_selection : out  STD_LOGIC_VECTOR (7 downto 0)
        );
end Top;

architecture Behavioral of Top is
signal segment_display_0_i :  std_logic_vector(6 downto 0);
signal segment_display_1_i :  std_logic_vector(6 downto 0);
signal segment_display_2_i :  std_logic_vector(6 downto 0);
signal segment_display_3_i :  std_logic_vector(6 downto 0);
signal segment_display_4_i :  std_logic_vector(6 downto 0);
signal segment_display_5_i :  std_logic_vector(6 downto 0);
signal segment_display_6_i :  std_logic_vector(6 downto 0);
signal segment_display_7_i :  std_logic_vector(6 downto 0);
signal clock_i :  std_logic;

 COMPONENT Display_Refresh
        PORT (
          clk_in : in  STD_LOGIC;
          segment_display_0 : IN std_logic_vector(6 downto 0);
		  segment_display_1 : IN std_logic_vector(6 downto 0);
		  segment_display_2 : IN std_logic_vector(6 downto 0);
		  segment_display_3 : IN std_logic_vector(6 downto 0);
		  segment_display_4 : IN std_logic_vector(6 downto 0);
		  segment_display_5 : IN std_logic_vector(6 downto 0);
		  segment_display_6 : IN std_logic_vector(6 downto 0);
		  segment_display_7 : IN std_logic_vector(6 downto 0);   
          display_number : out  STD_LOGIC_VECTOR (6 downto 0);
          display_selection : out  STD_LOGIC_VECTOR (7 downto 0)
         );
    END COMPONENT;
    
    COMPONENT Outs
        generic(
          NBitOpc:positive:=3 --El Numero de opciones son 5 por tanto vector de 3 bits sirve
            );
        Port (
              Enable: in std_logic;
              Opcion: in std_logic_vector (NBitOpc - 1 downto 0);
              segment_display_0 : OUT std_logic_vector(6 downto 0);
		      segment_display_1 : OUT std_logic_vector(6 downto 0);
		      segment_display_2 : OUT std_logic_vector(6 downto 0);
		      segment_display_3 : OUT std_logic_vector(6 downto 0);
		      segment_display_4 : OUT std_logic_vector(6 downto 0);
		      segment_display_5 : OUT std_logic_vector(6 downto 0);
		      segment_display_6 : OUT std_logic_vector(6 downto 0);
		      segment_display_7 : OUT std_logic_vector(6 downto 0)
            );
    END COMPONENT;
    
      COMPONENT CLK_Divider_2
        PORT (
                    Reset: in std_logic;
                    CLK_in: in std_logic;
                    CLK_out:out std_logic
         );
    END COMPONENT;
   

    
 begin
    Inst_CLK_Divider_2: CLK_Divider_2 PORT MAP (
        Reset => Reset,
        CLK_in=> Clk,
        CLK_out=>clock_i
        );
        
    Inst_Display_Refresh: Display_Refresh PORT MAP (
        clk_in => clock_i,
        segment_display_0 =>segment_display_0_i,
        segment_display_1 =>segment_display_1_i,
        segment_display_2 =>segment_display_2_i,
        segment_display_3 =>segment_display_3_i,
        segment_display_4 =>segment_display_4_i,
        segment_display_5 =>segment_display_5_i,
        segment_display_6 =>segment_display_6_i,
        segment_display_7 =>segment_display_7_i,
        display_number=>display_number,
        display_selection=>display_selection
        );
        
    Inst_Outs: Outs PORT MAP (
        Enable =>Enable,
        opcion =>opcion,
        segment_display_0 =>segment_display_0_i,
        segment_display_1 =>segment_display_1_i,
        segment_display_2 =>segment_display_2_i,
        segment_display_3 =>segment_display_3_i,
        segment_display_4 =>segment_display_4_i,
        segment_display_5 =>segment_display_5_i,
        segment_display_6 =>segment_display_6_i,
        segment_display_7 =>segment_display_7_i
        );

end Behavioral;
