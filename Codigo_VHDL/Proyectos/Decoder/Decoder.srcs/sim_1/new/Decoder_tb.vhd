----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 19.12.2019 13:19:26
-- Design Name: 
-- Module Name: Decoder_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Decoder_tb is
end Decoder_tb;

architecture behavior of Decoder_tb is 

  constant ClockPeriod      : time := 1 sec; 
  constant NBit:positive:=5;
  constant NLed:positive:=10;
  signal Enable:  std_logic;
  signal Numero: std_logic_vector( NBit - 1 downto 0);
  signal Led : std_logic_vector(NLed-1 downto 0);
 
    component Decoder is
        port (
                Enable: in std_logic;
                Numero: in std_logic_vector( NBit - 1 downto 0);
                Led : out std_logic_vector(NLed-1 downto 0)
             );
    end component;
begin
    uut: Decoder port map (
        Enable=>Enable,
        Numero=>Numero,
        Led=>Led
    );
    p1:process
    begin
     Enable<='1';
     for i in 0 to 19 loop
	      Numero <= std_logic_vector(to_unsigned(i, Numero'length));
	      wait for ClockPeriod;
     end loop;
      Enable<='0';
      Numero <=  std_logic_vector(to_unsigned(18, Numero'length)); --Comprobar que aunque carguemos numero no se encienden LEDs si no esta ENABLE activado
      wait for ClockPeriod;
      wait;
   end process;
    
end;