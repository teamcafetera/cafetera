----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 23.12.2019 13:03:36
-- Design Name: 
-- Module Name: PruebaTemp_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity PruebaTemp_tb is
end PruebaTemp_tb;

architecture Behavioral of PruebaTemp_tb is
  constant ClockPeriod      : time := 10 ns;
  constant seg: time := 1 sec;
  constant NBit:positive:=5;
  constant NLed:positive:=10;
  signal LOAD_TEMP:  std_logic;
  signal CLK:  std_logic;
  signal RESET:  std_logic;
  signal ENABLE_DECODER:  std_logic;
  signal TIEMPO_A_TEMPORIZAR: std_logic_vector( NBit - 1 downto 0);
  signal LED :  std_logic_vector(NLed-1 downto 0);
  signal READY_TEMP: std_logic;
  component PruebaTemp is
        port (
                  LOAD_TEMP:  in std_logic;
                  CLK:  in std_logic;
                  RESET:  in std_logic;
                  ENABLE_DECODER:  in std_logic;
                  TIEMPO_A_TEMPORIZAR:in std_logic_vector( NBit - 1 downto 0);
                  LED : out STD_LOGIC_VECTOR(9 downto 0);
                  READY_TEMP: out std_logic
             );
    end component;
begin
uut: PruebaTemp port map (
        LOAD_TEMP => LOAD_TEMP,
        CLK => CLK,
        RESET => RESET,
        ENABLE_DECODER => ENABLE_DECODER,
        TIEMPO_A_TEMPORIZAR=>TIEMPO_A_TEMPORIZAR,
        LED => LED,
        READY_TEMP=>READY_TEMP
    );
    
    
    p0: process
    begin
         CLK <= '0';
         wait for 0.5 * ClockPeriod;
         CLK<= '1';
          wait for 0.5 * ClockPeriod;
    end process;
    
    p1: process
    begin
     RESET<= '1', '0' after 0.25 * ClockPeriod;
     wait for 8*ClockPeriod;
     ENABLE_DECODER<='1';
     TIEMPO_A_TEMPORIZAR<="00101";
     LOAD_TEMP<='1','0' after 0.05 * ClockPeriod;
     wait for 7*seg;
     LOAD_TEMP<='1','0' after 0.05 * ClockPeriod;
     ENABLE_DECODER<='0';
     wait for 2.5*seg;
     ENABLE_DECODER<='1';
     wait;
    end process;
end Behavioral;
