----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 23.12.2019 12:08:20
-- Design Name: 
-- Module Name: PruebaTemp - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity PruebaTemp is
  generic(
     NBit:positive:=5;
     NLed:positive:=10
    );
    Port ( 
         LOAD_TEMP: in std_logic;
         CLK: in std_logic;
         RESET: in std_logic;
         ENABLE_DECODER: in std_logic;
         TIEMPO_A_TEMPORIZAR:in std_logic_vector( NBit - 1 downto 0);
         LED : out std_logic_vector(NLed - 1 downto 0);
         READY_TEMP:out std_logic--Se activa cuando la cuenta llega a un determinado numero de segundos
  );
end PruebaTemp;

architecture Behavioral of PruebaTemp is
  signal Seconds_i : std_logic_vector( NBit - 1 downto 0);
  signal clock:std_logic;
 
   COMPONENT CLK_Divider
        PORT (
                    Reset: in std_logic;
                    CLK_in: in std_logic;
                    CLK_out:out std_logic
         );
    END COMPONENT;
    
    
    COMPONENT Decoder
        generic(
                NBit:positive:=5;
                NLed:positive:=10
        );
        PORT (
                 Enable: in std_logic;
                 Numero: in std_logic_vector( NBit - 1 downto 0);
                 Led : out std_logic_vector(NLed-1 downto 0)
         );
    END COMPONENT;
    
    
    COMPONENT Temp
        generic(
                 NBit:positive:=5
        );
        Port ( 
                Load: in std_logic;
                Tiempo:in std_logic_vector( NBit - 1 downto 0);
                Clk: in std_logic;
                Reset: in std_logic;
                Seconds:out std_logic_vector( NBit - 1 downto 0);
                Ready:out std_logic--Se activa cuando la cuenta llega a un determinado numero de segundos
         );
    END COMPONENT;
    
    
    
    
begin


        Inst_CLK_Divider: CLK_Divider PORT MAP (
        Reset => RESET,
        CLK_in=> CLK,
        CLK_out=>clock
        );

        Inst_Decoder: Decoder PORT MAP (
        Enable => ENABLE_DECODER,
        Numero=> Seconds_i,
        Led=>LED
        );
        
        Inst_Temp: Temp PORT MAP (
        Load => LOAD_TEMP,
        Tiempo=> TIEMPO_A_TEMPORIZAR,
        Clk=>clock,
        Reset=>RESET,
        Seconds=>Seconds_i,
        Ready=>READY_TEMP
        );



end Behavioral;
