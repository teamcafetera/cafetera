----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 23.12.2019 12:14:12
-- Design Name: 
-- Module Name: Decoder - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Decoder is
    generic(
        NBit:positive:=5;
        NLed:positive:=10
    );
    Port ( 
        Enable: in std_logic;
        Numero: in std_logic_vector( NBit - 1 downto 0);
        Led : out std_logic_vector(NLed-1 downto 0)
    );
end Decoder;

architecture behavioral of Decoder is
begin
    process(Enable,Numero)  
    begin
        if Enable = '1' then 
            case (to_integer(unsigned(Numero))+1)  is
             when 1 => LED<="0000000001";
             when 2 => LED<="0000000011";
             when 3 => LED<="0000000111";
             when 4 => LED<="0000001111";
             when 5 => LED<="0000011111";
             when 6 => LED<="0000111111";
             when 7 => LED<="0001111111";
             when 8 => LED<="0011111111";
             when 9 => LED<="0111111111";
             when 10 => LED<="1111111111";
             when 11 => LED<="1111111111";
             when 12 => LED<="0111111111"; 
             when 13 => LED<="0011111111";  
             when 14 => LED<="0001111111";
             when 15 => LED<="0000111111";  
             when 16 => LED<="0000011111";  
             when 17 => LED<="0000001111";   
             when 18 => LED<="0000000111";   
             when 19 => LED<="0000000011";   
             when 20 => LED<="0000000001";  
             when others => LED<="0000000000";   
            end case; 
        else 
            LED<="0000000000";
        END IF;
      end process;       
   
end behavioral;
