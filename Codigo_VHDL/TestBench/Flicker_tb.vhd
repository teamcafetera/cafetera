----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02.01.2020 17:42:02
-- Design Name: 
-- Module Name: Flicker_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Flicker_tb is
--  Port ( );
end Flicker_tb;

architecture behavior of Flicker_tb is 
  constant ClockPeriod      : time := 1 sec; 
  constant NOpc:positive:=2;
  signal Enable:  std_logic;
  signal Clk: std_logic;
  signal Opcion: std_logic_vector (NOpc - 1 downto 0);
  signal Led : std_logic;
  
    component Flicker is
        generic(
                NOpc:positive:=2 --Numero de opciones
        );
        port (
                Enable: in std_logic;
                Clk: in std_logic;
                Opcion: in std_logic_vector (NOpc - 1 downto 0);
                Led : out std_logic
             );
    end component;
begin
    uut: Flicker port map (
        Enable=>Enable,
        Clk=>Clk,
        Opcion=>Opcion,
        Led=>Led
    );
    
    p0:process
    begin
      Clk<='0';
      wait for 0.5*ClockPeriod;
      Clk<='1';
      wait for 0.5*ClockPeriod;
    end process;
    
    p1:process
    begin
      Enable <= '0', '1' after 0.25 * ClockPeriod;
	  Opcion <= "01";
	  wait for 10*ClockPeriod;
	  Enable<='0';
	  wait for 2*ClockPeriod;
	  Enable<='1';
	  Opcion <= "10";
	  wait for 15*ClockPeriod;
	  Enable<='0';
      wait;
   end process;
    
end;