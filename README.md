

# CAFETERA BRET, JAVI Y JAIME



## CASOS DE USO

Para encender la cafetera habrá que darle al botón de START (en la FPGA el interruptor (SW1 [J15])
En este momento se encenderá un Led parpadeante indicando que la cafetera está encendida pero le falta agua
(en la FPGA el LED ) una vez introducida el agua el LED anterior se queda encendido y se enciende otro LED 
indicando que falta un cápsula (en la FPGA el LED ) la intruducción de cápsula se traduce en la placa como un interruptor
(en la FPGA el interruptor (SW )
Cuando la cafetera ya tiene el agua caliente y una cápsula se quedará esperando a que le pongas un vaso
(en la FPGA el LED )
Una vez encendida la cafetera se quedará esperando a que le introduzcas el agua,
una vez pulsado este interruptor se encenderá la luz verde de la cafetera (en la FPGA el LED0 (H17)), tras encenderse
la cafetera empieza a calentar agua (30 segundos y se encenderá en la plaza el LED1 (K15) indicador de que el agua está
caliente), una vez calentada el agua la cafetera esperará a que le introduzcas una capsula (un sensor en la realidad, 
representado en la FPGA como el LED2 (J13) que se enciende tras  5 segundos de encenderse la cafetera),  y pongas un vaso
en la base (un sensor en la realidad, representado en la FPGA como el LED3 (N14) que se enciende tras  10 segundos de 
encenderse la cafetera), una vez introducida la cápsula y el vaso en sus respectivos sitios, se pasará a la elección del
café (en este tipo de cafeteras hay dos botones para elegir entre mas/menos café) (en la FPGA se representan estos 2 
botones por 2 botones).

### SIMILITUDES FPGA-REALIDAD.
1. SW0 -> Interruptor de encendido/apagado de la cafetera.
2. LED0 (H17) -> Indicador de que la cafetera está encendida.
3. LED1(K15) -> Indicador de que el agua está caliente (se enciende tras 30 segundos).
4. LED2(J13) -> Indicador de cápsula introducida (Se enciende tras 5 segundos).
5. LED3(N14) -> Indicador de vaso introducido (Se enciende tras 5 segundos).
6. BOTÓN BTNL(P17) -> Botón para indicar que queremos un café corto
7. BOTÓN BTNR(M17) -> Botón para indicar que queremos café largo
8. TIRA LEDS (15-6) -> Sirviendo café 
9. LED3(N14) -> Indicador de vaso retirado (Se apaga tras 5 segundos de terminarse el café)
10. LED2(J13) -> Indicador cápsula retirada (Se apaga tras 5 segundos)


![Tabla FPGA-REALIDAD](images/Tabla.png)


   ![Representacion de pines utilizados en la tarjeta](images/Tarjeta.png)
   
   
   
   
## MODULOS
### TIMER

![Modulo Timer](images/Timer.png)

---














---

**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

--- 

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).